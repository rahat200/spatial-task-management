package com.sdproject.spatialtask.db;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.sdproject.spatialtask.domain.Employee;

public class EmployeeDAO {

	Context context;
	
	public EmployeeDAO(Context context) {
		this.context = context;
	}
	
	public List<Employee> getEmployeesList() {
		List<Employee> employeeList = new ArrayList<Employee>();
		for(int i = 0 ; i<=15; i++) {
			Employee employee = new Employee();
			
			employee.firstname = "Jonathon";
			employee.lastname = "Chris";
			employee.designation = "Field Officer";
			
			
			employeeList.add(employee);
		}
		return employeeList;
	}
}
