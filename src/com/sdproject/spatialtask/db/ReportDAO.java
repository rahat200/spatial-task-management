package com.sdproject.spatialtask.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.sdproject.spatialtask.domain.Report;
import com.sdproject.spatialtask.domain.Task;

public class ReportDAO {

	Context context;
	SQLiteHelper db;

	public ReportDAO(Context context) {
		this.context = context;
		db = SQLiteHelper.getInstance(context);
	}


	public void insertReport(Report report) {

		ContentValues vals = new ContentValues();
		vals.put(SQLiteHelper.REPORT_TASK_ID, report.task_id);
		vals.put(SQLiteHelper.REPORT_CREATE_TIME, report.createTime);
		vals.put(SQLiteHelper.REPORT_COMPLETE_TIME, report.completeTime);
		vals.put(SQLiteHelper.REPORT_MESSAGE, report.message);

		db.insert(SQLiteHelper.TABLE_REPORT, vals);
	}

	public List<Report> getReportList(String task_id) {
		List<Report> reportList = new ArrayList<Report>();

		String sql = "SELECT * FROM " + SQLiteHelper.TABLE_REPORT + " WHERE "
				+ SQLiteHelper.REPORT_TASK_ID + " = " + task_id;
		Cursor cursor = db.query(sql, null);

		if (cursor.moveToFirst()) {
			do {

				Report report = new Report();
				report.id = cursor.getInt(cursor.getColumnIndex(SQLiteHelper.ID));
				report.task_id = cursor.getInt(cursor
						.getColumnIndex(SQLiteHelper.REPORT_TASK_ID));
				report.createTime = cursor.getString(cursor
						.getColumnIndex(SQLiteHelper.REPORT_CREATE_TIME));
				report.completeTime = cursor.getString(cursor
						.getColumnIndex(SQLiteHelper.REPORT_COMPLETE_TIME));
				report.message = cursor.getString(cursor
						.getColumnIndex(SQLiteHelper.REPORT_MESSAGE));
				reportList.add(report);

			} while (cursor.moveToNext());
		}

		cursor.close();
		return reportList;
	}
}
