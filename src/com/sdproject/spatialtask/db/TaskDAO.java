package com.sdproject.spatialtask.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.sdproject.spatialtask.domain.Task;

public class TaskDAO {

	Context context;
	SQLiteHelper db;

	public TaskDAO(Context context) {
		this.context = context;
		db = SQLiteHelper.getInstance(context);
	}

	// public List<Task> getTasksList() {
	// List<Task> taskList = new ArrayList<Task>();
	// for (int i = 0; i <= 15; i++) {
	// Task task = new Task();
	// task.id = i;
	// task.message = "Lorem ipsum dolor sit amet, "
	// + "consectetuer adipiscing elit, sed diam ";
	// task.createTime = "02-Aug-2014";
	// task.latitude = 23.0083973;
	// task.longitude = 23.0083973;
	// task.deadline = "10-Aug-2014 ";
	//
	// taskList.add(task);
	// }
	// return taskList;
	// }
	//

	public void insertTask(Task task) {

		ContentValues vals = new ContentValues();
		vals.put(SQLiteHelper.TASK_LATTITUDE, task.latitude);
		vals.put(SQLiteHelper.TASK_LONGITUDE, task.longitude);
		vals.put(SQLiteHelper.TASK_MESSAGE, task.message);
		vals.put(SQLiteHelper.TASK_STATUS, task.status);
		vals.put(SQLiteHelper.TASK_DEADLINE, task.deadline);
		vals.put(SQLiteHelper.TASK_CREATE_TIME, task.createTime);
		vals.put(SQLiteHelper.TASK_ADDITIONAL_INFO, task.additionalInfo);

		db.insert(SQLiteHelper.TABLE_TASK, vals);
	}

	public void insertTaskList(List<Task> tasks) {

		for (Task task : tasks) {
			ContentValues vals = new ContentValues();
			vals.put(SQLiteHelper.TASK_LATTITUDE, task.latitude);
			vals.put(SQLiteHelper.TASK_LONGITUDE, task.longitude);
			vals.put(SQLiteHelper.TASK_MESSAGE, task.message);
			vals.put(SQLiteHelper.TASK_STATUS, task.status);
			vals.put(SQLiteHelper.TASK_DEADLINE, task.deadline);
			vals.put(SQLiteHelper.TASK_CREATE_TIME, task.createTime);
			vals.put(SQLiteHelper.TASK_ADDITIONAL_INFO, task.additionalInfo);

			long inserted =db.insert(SQLiteHelper.TABLE_TASK, vals);
			Log.e("Inserted at taskdao insertasklist" , " " +inserted);
			
		}
	}

	public List<Task> getTasksList(String status) {
		List<Task> taskList = new ArrayList<Task>();

		String sql = "SELECT * FROM " + SQLiteHelper.TABLE_TASK + " WHERE "
				+ SQLiteHelper.TASK_STATUS + " = " + status;
		Cursor cursor = db.query(sql, null);

		if (cursor.moveToFirst()) {
			do {

				Task tsk = new Task();
				tsk.id = cursor.getInt(cursor.getColumnIndex(SQLiteHelper.ID));
				tsk.latitude = cursor.getInt(cursor
						.getColumnIndex(SQLiteHelper.TASK_LATTITUDE));
				tsk.longitude = cursor.getInt(cursor
						.getColumnIndex(SQLiteHelper.TASK_LONGITUDE));
				tsk.message = cursor.getString(cursor
						.getColumnIndex(SQLiteHelper.TASK_MESSAGE));
				tsk.status = cursor.getString(cursor
						.getColumnIndex(SQLiteHelper.TASK_STATUS));
				tsk.deadline = cursor.getString(cursor
						.getColumnIndex(SQLiteHelper.TASK_DEADLINE));
				tsk.createTime = cursor.getString(cursor
						.getColumnIndex(SQLiteHelper.TASK_CREATE_TIME));
				tsk.additionalInfo = cursor.getString(cursor
						.getColumnIndex(SQLiteHelper.TASK_ADDITIONAL_INFO));
				taskList.add(tsk);

			} while (cursor.moveToNext());
		}

		cursor.close();
		return taskList;
	}

	public Task getTask(int id) {
		Task tsk = new Task();

		String sql = "SELECT * FROM " + SQLiteHelper.TABLE_TASK
				+ " WHERE id = " + id;
		Cursor cursor = db.query(sql, null);

		if (cursor.moveToFirst()) {
			tsk.id = cursor.getInt(cursor.getColumnIndex(SQLiteHelper.ID));
			tsk.latitude = cursor.getInt(cursor
					.getColumnIndex(SQLiteHelper.TASK_LATTITUDE));
			tsk.longitude = cursor.getInt(cursor
					.getColumnIndex(SQLiteHelper.TASK_LONGITUDE));
			tsk.message = cursor.getString(cursor
					.getColumnIndex(SQLiteHelper.TASK_MESSAGE));
			tsk.status = cursor.getString(cursor
					.getColumnIndex(SQLiteHelper.TASK_STATUS));
			tsk.deadline = cursor.getString(cursor
					.getColumnIndex(SQLiteHelper.TASK_DEADLINE));
			tsk.createTime = cursor.getString(cursor
					.getColumnIndex(SQLiteHelper.TASK_CREATE_TIME));
			tsk.additionalInfo = cursor.getString(cursor
					.getColumnIndex(SQLiteHelper.TASK_ADDITIONAL_INFO));
		}

		cursor.close();
		return tsk;
	}

	public void updateTask(Task task) {
		Task tsk = new Task();

		ContentValues vals = new ContentValues();
		vals.put(SQLiteHelper.TASK_LATTITUDE, task.latitude);
		vals.put(SQLiteHelper.TASK_LONGITUDE, task.longitude);
		vals.put(SQLiteHelper.TASK_MESSAGE, task.message);
		vals.put(SQLiteHelper.TASK_STATUS, task.status);
		vals.put(SQLiteHelper.TASK_DEADLINE, task.deadline);
		vals.put(SQLiteHelper.TASK_CREATE_TIME, task.createTime);
		vals.put(SQLiteHelper.TASK_ADDITIONAL_INFO, task.additionalInfo);

		String where = SQLiteHelper.ID + "=?"; // The where clause to identify
												// which columns to update.
		String[] value = { "" + task.id }; // The value for the where clause.

		// Update the database (all columns in TABLE_NAME where my_column has a
		// value of 2 will be changed to 5)
		db.update(SQLiteHelper.TABLE_TASK, vals, where, value);

	}
}
