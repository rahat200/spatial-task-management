package com.sdproject.spatialtask.db;

import java.io.File;

import com.sdproject.spatialtask.utilities.AppConstants;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLiteHelper {

	private static final int DATABASE_VERSION = 1;

	public static final String TABLE_EMPLOYEE = "employee";
	public static final String TABLE_REPORT = "report";
	public static final String TABLE_EMPLOYEE_TASK = "employee_task";
	public static final String TABLE_TASK = "task";
	public static final String TABLE_USER = "user";

	public static final String ID = "id";
	public static final String EMPLOYEE_USER_ID = "user_id";
	public static final String EMPLOYEE_FIRSTNAME = "firstname";
	public static final String EMPLOYEE_LASTNAME = "lastname";
	public static final String EMPLOYEE_DATE_OF_BIRTH = "date_of_birth";
	public static final String EMPLOYEE_DESIGNATION = "designation";
	public static final String EMPLOYEE_LONGITUDE = "longitude";
	public static final String EMPLOYEE_LATTITUDE = "lattitude";

	public static final String TASK_LATTITUDE = "lattitude";
	public static final String TASK_LONGITUDE = "longitude";
	public static final String TASK_MESSAGE = "message";
	public static final String TASK_STATUS = "status";
	public static final String TASK_DEADLINE = "deadline";
	public static final String TASK_CREATE_TIME = "create_time";
	public static final String TASK_ADDITIONAL_INFO = "additional_info";
	public static final String TASK_PRIORITY = "priority";

	public static final String REPORT_TASK_ID = "task_id";
	public static final String REPORT_COMPLETE_TIME = "complete_time";
	public static final String REPORT_CREATE_TIME = "create_time";
	public static final String REPORT_MESSAGE = "message";

	public static final String EMPLOYEE_TASK__EMP = "emp_uid";
	public static final String EMPLOYEE_TASK_TSK = "task_id";

	public static final String USER_EMAIL = "email";
	public static final String USER_PASSWORD = "password";

	private static String DB_PATH;

	private static SQLiteHelper dbHelper = null;

	private static SQLiteDatabase db = null;

	OpenHelper openHelper;
	private Context context;
	static String query;

	
	static String employeeSql = "CREATE TABLE IF NOT EXISTS " + TABLE_EMPLOYEE
			+ "(" + ID + " INTEGER ," 
			+ EMPLOYEE_USER_ID+ " INTEGER, "
			+ EMPLOYEE_FIRSTNAME+ " TEXT, "
			+ EMPLOYEE_LONGITUDE+ " REAL, "
			+ EMPLOYEE_LATTITUDE+ " REAL, "
			+ EMPLOYEE_LASTNAME+ " TEXT, "
			+ EMPLOYEE_DATE_OF_BIRTH+" TEXT, "
			+ EMPLOYEE_DESIGNATION+ " TEXT );";
	
	static String taskSql = "CREATE TABLE IF NOT EXISTS " + TABLE_TASK
			+ "(" + ID + " INTEGER ," 
			+ TASK_LATTITUDE+ " REAL, "
			+ TASK_LONGITUDE+ " REAL, "
			+ TASK_MESSAGE+ " TEXT, "
			+ TASK_STATUS+" TEXT, "
			+ TASK_DEADLINE+" TEXT, "
			+ TASK_CREATE_TIME+" TEXT, "
			+ TASK_PRIORITY+" INTEGER, "			
			+ TASK_ADDITIONAL_INFO+ " TEXT );";

	static String reportSql = "CREATE TABLE IF NOT EXISTS " + TABLE_REPORT
			+ "(" + ID + " INTEGER ," 
			+ REPORT_TASK_ID+ " INTEGER, "
			+ REPORT_COMPLETE_TIME+ " TEXT, "
			+ REPORT_CREATE_TIME+ " TEXT, "
			+ REPORT_MESSAGE+ " TEXT );";

	static String employeeTaskSql = "CREATE TABLE IF NOT EXISTS " + TABLE_EMPLOYEE_TASK
			+ "(" + ID + " INTEGER ," 
			+ EMPLOYEE_TASK__EMP+ " INTEGER, "
			+ EMPLOYEE_TASK_TSK+ " INTEGER );";
	
	static String userSql = "CREATE TABLE IF NOT EXISTS " + TABLE_USER
			+ "(" + ID + " INTEGER ," 
			+ USER_EMAIL+ " TEXT, "
			+ USER_PASSWORD+ " TEXT );";
	
	private SQLiteHelper(Context context) {
		this.context = context;

		DB_PATH = new File(AppConstants.getDatabasePath(context),
				AppConstants.DB_NAME).getAbsolutePath();

		if (db != null)
			if (db.isOpen())
				db.close();

		openHelper = new OpenHelper(this.context);
		db = openHelper.getWritableDatabase();

		dbHelper = this;

	}

	public void beginTrans() {
		db.beginTransaction();
	}

	public void endTrans() {
		db.endTransaction();
	}

	public static SQLiteHelper getInstance(Context context) {
		if (dbHelper == null) {
			dbHelper = new SQLiteHelper(context);
		}
		return dbHelper;
	}

	public synchronized void close() {
		if (openHelper != null) {
			openHelper.close();
		}
	}

	public synchronized long insert(String table, ContentValues values) {
		return db.insert(table, null, values);
	}

	public synchronized long insertOrIgnore(String table, ContentValues values) {
		return db.insertWithOnConflict(table, null, values,
				SQLiteDatabase.CONFLICT_IGNORE);
	}

	public synchronized int delete(String table, String whereClause,
			String[] whereArgs) {
		return db.delete(table, whereClause, whereArgs);
	}

	public synchronized int update(String table, ContentValues values,
			String whereClause, String[] whereArgs) {
		return db.update(table, values, whereClause, whereArgs);
	}

	public Cursor query(String sql, String[] whereValues) {
		return db.rawQuery(sql, whereValues);
	}

	public boolean exist(String sql, String[] whereValues) {
		Cursor foo = db.rawQuery(sql, whereValues);

		if (foo != null && foo.getCount() > 0) {
			foo.close();
			return true;
		}
		foo.close();
		return false;
	}

	static class OpenHelper extends SQLiteOpenHelper {
		OpenHelper(Context context) {
			super(context, DB_PATH, null, DATABASE_VERSION);
		}

		public void onCreate(SQLiteDatabase db) {

			
			
			
			
			db.execSQL(employeeSql);
			db.execSQL(taskSql);
			db.execSQL(reportSql);
			db.execSQL(employeeTaskSql);
			db.execSQL(userSql);

		}

		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		}

	}

}
