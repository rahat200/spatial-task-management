package com.sdproject.spatialtask.domain;

public class DrawerItem {

	public String title;
	public int icon_id;
	public int count = 0;
	public boolean isCounterVisible = false;
}
