package com.sdproject.spatialtask.domain;

public class Employee {
	public int id;
	public int userId;
	public String firstname;
	public String lastname;
	public String designation;
	public String dateOfBirth;
	public double lng;
	public double lat;
}
