package com.sdproject.spatialtask.domain;

public class Task {

	public int id;
	public String message;
	public String createTime;
	public String deadline;
	public String status;
	public String additionalInfo;
	public double latitude;
	public double longitude;
	public int priority;
	
}
