package com.sdproject.spatialtask.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.sdproject.spatialtask.R;
import com.sdproject.spatialtask.domain.Task;
import com.sdproject.spatialtask.fragments.ManagerDetailsTaskFragment;
import com.sdproject.spatialtask.fragments.ManagerOtherEmployeeList;
import com.sdproject.spatialtask.utilities.AppConstants;



public class TaskListAdapter extends ArrayAdapter<Task> {

	Context mContext;

	List<Task> tasksList;

	String mTaskType, designation;

	public TaskListAdapter(Context context, String taskType) {
		super(context, R.layout.list_row_task);
		this.mContext = context;
		mTaskType = taskType;
		tasksList = new ArrayList<Task>();

	}

	public void setTaskList(List<Task> list) {
		this.tasksList = list;
	}

	@Override
	public int getCount() {
		return this.tasksList.size();
	}

	@Override
	public Task getItem(int i) {
		return tasksList.get(i);
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {

		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.list_row_task, null);

			ViewHolder h = new ViewHolder();
			h.description = (TextView) view.findViewById(R.id.tv_employee_name);
			h.priority = (TextView) view.findViewById(R.id.tv_designation);
			h.deadline = (TextView) view.findViewById(R.id.tv_availability);
			view.setTag(h);
		}

		ViewHolder holder = (ViewHolder) view.getTag();

		final Task task = tasksList.get(position);

		holder.description.setText(task.message);
		
		if(task.priority > 5) {
			holder.priority.setText("priority: high");
		}
		else if(task.priority > 3) {
			holder.priority.setText("priority: medium");
		}
		else {
			holder.priority.setText("priority: low");
		}
		
		//holder.priority.setText(task.createTime);
		holder.deadline.setText("deadline: "+task.deadline);
		
		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

//				designation = AppConstants.getStringSharedPreference(mContext,
//						AppConstants.SP_DESIGNATION);
//				/* it can be valid for manager only */
//				if (mTaskType.equals(AppConstants.TASK_UNASSIGNED)
//						&& designation.equals(AppConstants.EMP_MANAGER)) {
//
//					ManagerOtherEmployeeList empFrag = new ManagerOtherEmployeeList();
//					Bundle args = new Bundle();
//					args.putInt(AppConstants.IN_TASK_ID, task.id);
//					empFrag.setArguments(args);
//					
//
//					FragmentManager fragmentManager = ((SherlockFragmentActivity) mContext)
//							.getSupportFragmentManager();
//					fragmentManager.beginTransaction()
//							.replace(R.id.fragment_container, empFrag).commit();
//				}
//				else if (mTaskType.equals(AppConstants.TASK_ASSIGNED)
//						&& designation.equals(AppConstants.EMP_MANAGER)) {
//					
					ManagerDetailsTaskFragment taskDetailFrag = new ManagerDetailsTaskFragment();
					Bundle args = new Bundle();					
					args.putInt(AppConstants.IN_TASK_ID, task.id);
					args.putString(AppConstants.IN_TASK_MESSAGE, task.message);
					args.putString(AppConstants.IN_TASK_CREATE_TIME, task.createTime);
					args.putString(AppConstants.IN_TASK_DEADLINE, task.deadline);
					args.putString(AppConstants.IN_TASK_STATUS, task.status);
					args.putString(AppConstants.IN_TASK_ADD_INFO, task.additionalInfo);
					args.putDouble(AppConstants.IN_TASK_LATTITUDE, task.latitude);
					args.putDouble(AppConstants.IN_TASK_LONGITUDE, task.longitude);
					
					
					taskDetailFrag.setArguments(args);
					
					FragmentManager fragmentManager = ((SherlockFragmentActivity) mContext)
							.getSupportFragmentManager();
					fragmentManager.beginTransaction()
							.replace(R.id.fragment_container, taskDetailFrag).commit();
					
//				}
//	
//				
////				Toast.makeText(mContext, mTaskType+" task.id = "+task.id+"   "+designation, Toast.LENGTH_LONG).show();
			}
		});

		return view;

	}

}

class ViewHolder {
	TextView description;
	TextView priority;
	TextView deadline;
}
