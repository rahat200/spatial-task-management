package com.sdproject.spatialtask.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sdproject.spatialtask.R;
import com.sdproject.spatialtask.domain.DrawerItem;

public class DrawerListAdapter extends ArrayAdapter<DrawerItem>{

	private Context context;
    private ArrayList<DrawerItem> navDrawerItems;
     
    public DrawerListAdapter(Context context, ArrayList<DrawerItem> navDrawerItems){
        super(context, R.layout.list_row_drawer);
    	this.context = context;
        this.navDrawerItems = navDrawerItems;
    }
 
    @Override
    public int getCount() {
        return navDrawerItems.size();
    }
 
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.list_row_drawer, null);
        }
          
        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.iv_drawer_item_icon);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.tv_drawer_item_title);
        TextView txtCount = (TextView) convertView.findViewById(R.id.tv_drawer_item_counter);
          
        imgIcon.setImageResource(navDrawerItems.get(position).icon_id);        
        txtTitle.setText(navDrawerItems.get(position).title);
         
        // displaying count
        // check whether it set visible or not
        if(navDrawerItems.get(position).isCounterVisible){
            txtCount.setText(""+navDrawerItems.get(position).count);
        }else{
            // hide the counter view
            txtCount.setVisibility(View.GONE);
        }
         
        return convertView;
    }
 
}
