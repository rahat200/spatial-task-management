package com.sdproject.spatialtask.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.sdproject.spatialtask.R;
import com.sdproject.spatialtask.domain.Employee;
import com.sdproject.spatialtask.http.AssignedTask;

public class EmployeeListAdapter extends ArrayAdapter<Employee> {

	Context mContext;
	int mTaskID;
	List<Employee> employeesList;

	public EmployeeListAdapter(Context context, int taskID) {
		super(context, R.layout.list_row_employee);
		this.mContext = context;
		this.mTaskID = taskID;
		employeesList = new ArrayList<Employee>();

	}

	public void setEmployeeList(List<Employee> list) {
		this.employeesList = list;
	}

	@Override
	public int getCount() {
		return this.employeesList.size();
	}

	@Override
	public Employee getItem(int i) {
		return employeesList.get(i);
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {

		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.list_row_employee, null);

			ViewHolder h = new ViewHolder();
			h.designation = (TextView) view.findViewById(R.id.tv_designation);
			h.name = (TextView) view.findViewById(R.id.tv_employee_name);
			h.avaiability = (TextView) view.findViewById(R.id.tv_availability);
			view.setTag(h);
		}

		ViewHolder holder = (ViewHolder) view.getTag();

		final Employee employee = employeesList.get(position);

		holder.designation.setText(employee.designation);
		holder.name.setText(employee.firstname + " " + employee.lastname);
		holder.avaiability.setText("available");
		/* it can be valid for manager only */
		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mTaskID != 0) {
					new AssignedTask(mContext).execute(employee.userId + "",
							mTaskID + "");
				}
			}
		});
		return view;

	}

	class ViewHolder {
		TextView name;
		TextView designation;
		TextView avaiability;
	}
}
