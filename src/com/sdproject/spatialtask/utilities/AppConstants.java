package com.sdproject.spatialtask.utilities;

import java.io.File;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

public class AppConstants {

	
	public static String DB_NAME = "spatialtask.db";
	public static String BASE_URL = "http://class-note.com/spatial/index.php/";
	
	/* shared preference constant */
	public static final String SP_USER_ID = "user_id";
	public static final String SP_USER_NAME = "user_name";
	public static final String SP_LOGGED_IN= "logged_in";
	public static final String SP_DESIGNATION= "designation";
	public static final String SP_POPULATE_LOCAL_TASK_DB= "first_time";
	
	/* task constant*/
	public static final String TASK_PENDING= "pending";
	public static final String TASK_ASSIGNED= "assigned";
	public static final String TASK_UNASSIGNED= "unassigned";
	public static final String TASK_COMPLETED= "completed";
	
	/* employee constant*/
	public static final String EMP_MANAGER= "manager";
	public static final String EMP_EMPLOYEE= "employee";
	
	/* task intent constant */
	public static final String IN_TASK_ID= "task_id";
	public static final String IN_TASK_LONGITUDE= "longitude";
	public static final String IN_TASK_LATTITUDE= "lattitude";
	public static final String IN_TASK_ADD_INFO = "add_info";
	public static final String IN_TASK_STATUS = "status";
	public static final String IN_TASK_DEADLINE = "deadline";
	public static final String IN_TASK_MESSAGE = "message";
	public static final String IN_TASK_CREATE_TIME = "create_time";
	
	public static File getDatabasePath(Context context) {
		return context.getExternalFilesDir(null);
	}
	
	
	/* shared preference functions */
	public static final void setStringSharedPreference(Context context, String key, String value) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(key, value);		
		editor.apply();
	}
	public static final void setIntSharedPreference(Context context, String key, int value) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(key, value);		
		editor.apply();
	}
	
	public static final void setBooleanSharedPreference(Context context, String key, Boolean value) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean(key, value);		
		editor.apply();
	}

	public static final String getStringSharedPreference(Context context, String key) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		return preferences.getString(key, "");

	}
	
	public static final int getIntSharedPreference(Context context, String key) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		return preferences.getInt(key, 0);

	}
	public static final Boolean getBooleanSharedPreference(Context context, String key) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		return preferences.getBoolean(key, false);

	}
}
