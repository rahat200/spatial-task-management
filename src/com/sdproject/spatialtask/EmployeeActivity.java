package com.sdproject.spatialtask;

import java.util.ArrayList;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.sdproject.spatialtask.adapters.DrawerListAdapter;
import com.sdproject.spatialtask.domain.DrawerItem;
import com.sdproject.spatialtask.fragments.EmployeeAssignedTask;
import com.sdproject.spatialtask.fragments.EmployeeOtherEmployeeList;
import com.sdproject.spatialtask.fragments.ManagerAssignedTask;
import com.sdproject.spatialtask.fragments.EmployeeCompletedTask;
import com.sdproject.spatialtask.fragments.CreateTaskFragment;
import com.sdproject.spatialtask.fragments.ManagerOtherEmployeeList;
import com.sdproject.spatialtask.fragments.ManagerOtherEmployeeLocation;
import com.sdproject.spatialtask.fragments.EmployeePendingTask;
import com.sdproject.spatialtask.utilities.AppConstants;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class EmployeeActivity extends SherlockFragmentActivity {

	private DrawerLayout drawerLayout;
	private ListView drawerList;
	private ActionBarDrawerToggle drawerToggle;

	private CharSequence drawerTitle;

	private CharSequence appTitle;

	private String[] drawerMenuTitles;
	private TypedArray drawerMenuIcons;

	private ArrayList<DrawerItem> drawerItems;
	private DrawerListAdapter adapter;
	Context context;
	int USER_ID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_employee);
		context = EmployeeActivity.this;

//		Toast.makeText(this, "EmployeeActivity", Toast.LENGTH_SHORT).show();
		
		USER_ID = AppConstants.getIntSharedPreference(EmployeeActivity.this,
				AppConstants.SP_USER_ID);

		initializeDrawerItems();

		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerList = (ListView) findViewById(R.id.lv_drawer);

		setDrawerHeader();
		setDrawerFooter();

		appTitle = drawerTitle = getTitle();

		adapter = new DrawerListAdapter(this, drawerItems);
		drawerList.setAdapter(adapter);
		drawerList.setOnItemClickListener(new DrawerMenuClickListener());

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
				R.drawable.ic_drawer, 
				R.string.app_name, 
				R.string.app_name
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(appTitle);
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				
				getActionBar().setTitle("Employee Menu");
				
				invalidateOptionsMenu();
			}
		};
		drawerLayout.setDrawerListener(drawerToggle);
		if (savedInstanceState == null) {
			// on first time display view for first nav item
			displayFragment(1);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//MenuInflater inflater = getSupportMenuInflater();
		//inflater.inflate(R.menu.menu_task, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(
			com.actionbarsherlock.view.MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			if (drawerLayout.isDrawerOpen(drawerList)) {
				drawerLayout.closeDrawer(drawerList);
			} else {
				drawerLayout.openDrawer(drawerList);
			}
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void setTitle(CharSequence title) {
		appTitle = title;
		getActionBar().setTitle(appTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		drawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		drawerToggle.onConfigurationChanged(newConfig);
	}

	private class DrawerMenuClickListener implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			displayFragment(position);
		}
	}

	
	private void displayFragment(int position) {

		SherlockFragment fragment = null;

		switch (position) {

		case 1:
			fragment = new EmployeeAssignedTask();
			break;
		case 2:
			fragment = new EmployeePendingTask();
			break;
		case 3:
			fragment = new EmployeeCompletedTask();
			break;
		case 4:
			fragment = new EmployeeOtherEmployeeList();
			break;

		default:
			break;
		}

		if (fragment != null) {
			
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.fragment_container, fragment).commit();

			// update selected item and title, then close the drawer
			drawerList.setItemChecked(position, true);
			drawerList.setSelection(position);
			setTitle(drawerMenuTitles[position-1]);
			drawerLayout.closeDrawer(drawerList);
		
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}

	private void initializeDrawerItems() {
	
		drawerMenuTitles = getResources().getStringArray(
				R.array.drawer_items_employee);
		drawerMenuIcons = getResources().obtainTypedArray(
				R.array.drawer_icons_employee);

		drawerItems = new ArrayList<DrawerItem>();

		for (int i = 0; i < drawerMenuTitles.length; i++) {
			DrawerItem item = new DrawerItem();
			item.title = drawerMenuTitles[i];
			item.icon_id = drawerMenuIcons.getResourceId(i, -1);
			item.count = i + 3;
			item.isCounterVisible = true;

			drawerItems.add(item);
		}

		drawerMenuIcons.recycle();

	}

	private void setDrawerHeader() {

		View header_view = LayoutInflater.from(this).inflate(
				R.layout.drawer_header, null);

		((ImageView) header_view.findViewById(R.id.iv_employee_thumb))
				.setImageBitmap(BitmapFactory.decodeResource(getResources(),
						R.drawable.dummy_man));

		((TextView) header_view.findViewById(R.id.tv_employee_name))
				.setText(AppConstants.getStringSharedPreference(this,
						AppConstants.SP_USER_NAME));

		((TextView) header_view.findViewById(R.id.tv_designation))
				.setText(AppConstants.getStringSharedPreference(this,
						AppConstants.SP_DESIGNATION));

		drawerList.addHeaderView(header_view);
	}

	private void setDrawerFooter() {
		View footer_view = LayoutInflater.from(this).inflate(
				R.layout.drawer_footer, null);
		drawerList.addFooterView(footer_view);
	}

	public void onSignoutClicked(View v) {
		AppConstants.setBooleanSharedPreference(context,
				AppConstants.SP_LOGGED_IN, false);
		AppConstants.setIntSharedPreference(context,
				AppConstants.SP_USER_ID,0);
		AppConstants.setStringSharedPreference(context,
				AppConstants.SP_USER_NAME,"");
		AppConstants.setStringSharedPreference(context,
				AppConstants.SP_DESIGNATION,"");
		Intent in = new Intent(context,LoginActivity.class);
		in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK); 
		startActivity(in);
		finish();
		
	}
	
	public void onCreateTaskClicked(View v){
		
		SherlockFragment fragment= new CreateTaskFragment(); 
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.fragment_container, fragment).commit();
		// update selected item and title, then close the drawer
		setTitle(getResources().getString(R.string.prompt_create_task));
		drawerLayout.closeDrawer(drawerList);
	}


}
