package com.sdproject.spatialtask.http;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.internal.co;
import com.sdproject.spatialtask.R;
import com.sdproject.spatialtask.adapters.EmployeeListAdapter;
import com.sdproject.spatialtask.adapters.TaskListAdapter;
import com.sdproject.spatialtask.db.TaskDAO;
import com.sdproject.spatialtask.domain.Employee;
import com.sdproject.spatialtask.domain.Task;
import com.sdproject.spatialtask.utilities.AppConstants;
import com.sdproject.spatialtask.utilities.ServiceHandler;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Async task class to get json by making HTTP call
 * */
public class GetEmployees extends AsyncTask<String, String, String> {

	public ProgressDialog pDialog;

	// JSONArray
	JSONArray tasks = null;

	// URL to get contacts JSON
	private static String url = AppConstants.BASE_URL+"Api_employee/employee/";

	// 	http://class-note.com/spatial/index.php/Api_employee/employee?tag=all_employee&designation=manager

	// JSONArray
	JSONArray contacts = null;

	public static Context context;
	public static ListView empListView;

	public static final String TAG = "tag";
	public static final String TAG_DETAILS = "details";
	public static final String TAG_EMPLOYEE = "employees";
	
	public static final String TAG_ID = "id";
	public static final String TAG_ALL_EMP = "all_employee";
	public static final String TAG_UID = "user_id";
	public static final String TAG_LASTNAME = "lastname";
	public static final String TAG_FIRSTNAME = "firstname";
	public static final String TAG_BIRTH_DATE = "date_of_birth";
	public static final String TAG_DESIGNATION = "designation";
	public static final String TAG_LONGITUDE = "longitude";	
	public static final String TAG_LATTITUDE = "lattitude";	
	public static final String TAG_SUCCESS = "success";	
	int valid,taskID;
	List<Employee> empList = new ArrayList<Employee>();
	Employee emp;

	public GetEmployees(Context mContext, ListView mEmpListView,int mTaskID) {
		context = mContext;
		empListView = mEmpListView;
		taskID = mTaskID;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		// Showing progress dialog
		pDialog = new ProgressDialog(context);
		pDialog.setMessage("Please wait...");
		pDialog.setCancelable(false);
		pDialog.show();

	}

	@Override
	protected String doInBackground(String... args) {
		// Creating service handler class instance
		ServiceHandler sh = new ServiceHandler();

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(TAG, TAG_ALL_EMP));
		params.add(new BasicNameValuePair(TAG_DESIGNATION, args[0]));

		// Making a request to url and getting response
		String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, params);

		if (jsonStr != null) {
			try {
				JSONObject jsonObj = new JSONObject(jsonStr);

				valid = jsonObj.getInt(TAG_SUCCESS);
				if (valid == 1) {
					contacts = jsonObj.getJSONArray(TAG_EMPLOYEE);
					if (contacts.length() > 0) {
						// looping through All Contacts
						for (int i = 0; i < contacts.length(); i++) {
							JSONObject c = contacts.getJSONObject(i);
							emp = new Employee();
							emp.id=c.getInt(TAG_ID);
							emp.firstname = c.getString(TAG_FIRSTNAME);
							emp.lastname = c.getString(TAG_LASTNAME);
							emp.dateOfBirth = c.getString(TAG_BIRTH_DATE);
							emp.userId = c.getInt(TAG_UID);
							emp.designation = c.getString(TAG_DESIGNATION);
							emp.lng = c.getDouble(TAG_LONGITUDE);
							emp.lat = c.getDouble(TAG_LATTITUDE);
							empList.add(emp);
						}
					}
				}
				// Getting JSON Array node

			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			Log.e("ServiceHandler", "Couldn't get any data from the url");
			/* Search Local db if net is not available */
		}

		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		// Dismiss the progress dialog
		if (pDialog.isShowing())
			pDialog.dismiss();

		if(valid == 1){
			if(!empList.isEmpty()){
				EmployeeListAdapter adapter = new EmployeeListAdapter(context,taskID);
				adapter.setEmployeeList(empList);			
				empListView.setAdapter(adapter);
			}
			else{
				Toast.makeText(context, R.string.empty_employee, Toast.LENGTH_LONG).show();
			}
		}else{
			Toast.makeText(context, R.string.failed_fetch_data, Toast.LENGTH_LONG).show();
		}
	}
}
