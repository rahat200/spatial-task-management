package com.sdproject.spatialtask.http;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.internal.co;
import com.sdproject.spatialtask.R;
import com.sdproject.spatialtask.adapters.TaskListAdapter;
import com.sdproject.spatialtask.db.TaskDAO;
import com.sdproject.spatialtask.domain.Employee;
import com.sdproject.spatialtask.domain.Task;
import com.sdproject.spatialtask.utilities.AppConstants;
import com.sdproject.spatialtask.utilities.ServiceHandler;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Async task class to get json by making HTTP call
 * */
public class GetEmployeeTasks extends AsyncTask<String, String, String> {

	public ProgressDialog pDialog;

	// JSONArray
	JSONArray tasks = null;

	// URL to get contacts JSON
	private static String url = AppConstants.BASE_URL
			+ "Api_employee_task/employee_task/";

	// http://class-note.com/spatial/index.php/Api_task/task?tag=all_task&status=pending

	// JSONArray
	JSONArray contacts = null;

	public static Context context;
	public static ListView taskListView;

	public static final String TAG = "tag";
	public static final String TAG_ID = "id";
	public static final String TAG_DETAILS = "details";
	public static final String TAG_GET_TASK = "get_task";
	public static final String TAG_EMP_ID = "emp_id";
	public static final String TAG_LAT = "lattitude";
	public static final String TAG_LNG = "longitude";
	public static final String TAG_MSG = "message";
	public static final String TAG_STATUS = "status";
	public static final String TAG_DEADLINE = "deadline";
	public static final String TAG_CREATE_TIME = "create_time";
	public static final String TAG_INFO = "additional_info";
	public static final String TAG_SUCCESS = "success";
	public static final String TAG_TASKS = "tasks";
	public static final String TAG_PRIORITY = "priority";

	int valid;
	String taskType, empID;
	List<Task> taskList = new ArrayList<Task>();

	public GetEmployeeTasks(Context mContext, ListView mTaskListView) {
		context = mContext;
		taskListView = mTaskListView;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		// Showing progress dialog
		pDialog = new ProgressDialog(context);
		pDialog.setMessage("Please wait...");
		pDialog.setCancelable(false);
		pDialog.show();

	}

	@Override
	protected String doInBackground(String... args) {
		// Creating service handler class instance
		ServiceHandler sh = new ServiceHandler();

		empID = args[0];
		taskType = args[1];
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(TAG, TAG_GET_TASK));
		params.add(new BasicNameValuePair(TAG_EMP_ID, empID));
		params.add(new BasicNameValuePair(TAG_STATUS, taskType));

		// Making a request to url and getting response
		String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, params);

		if (jsonStr != null) {
			try {
				JSONObject jsonObj = new JSONObject(jsonStr);

				valid = jsonObj.getInt(TAG_SUCCESS);
				Log.e("get employee http", "" + valid);
				if (valid == 1) {
					// TASK IS CREATED
					tasks = jsonObj.getJSONArray(TAG_DETAILS);
					if (tasks.length() > 0) {
						// looping through All Contacts
						for (int i = 0; i < tasks.length(); i++) {
							if (!tasks.isNull(i)) {
								JSONObject c = tasks.getJSONObject(i);
								Task tmp = new Task();
								tmp.id = c.getInt(TAG_ID);
								tmp.latitude = c.getDouble(TAG_LAT);
								tmp.longitude = c.getDouble(TAG_LNG);
								tmp.message = c.getString(TAG_MSG);
								tmp.status = c.getString(TAG_STATUS);
								tmp.deadline = c.getString(TAG_DEADLINE);
								tmp.createTime = c.getString(TAG_CREATE_TIME);
								tmp.priority = c.getInt(TAG_PRIORITY);
								tmp.additionalInfo = c.getString(TAG_INFO);
								taskList.add(tmp);
							}
						}
					}
				}
				// Getting JSON Array node

			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			Log.e("ServiceHandler", "Couldn't get any data from the url");
			/* Search Local db if net is not available */
		}

		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		// Dismiss the progress dialog
		if (pDialog.isShowing())
			pDialog.dismiss();

		if (valid == 1) {
			if (!taskList.isEmpty()) {
				if (!AppConstants.getBooleanSharedPreference(context,
						AppConstants.SP_POPULATE_LOCAL_TASK_DB)) {
					AppConstants.setBooleanSharedPreference(context,
							AppConstants.SP_POPULATE_LOCAL_TASK_DB, true);
					TaskDAO taskDao = new TaskDAO(context);
					taskDao.insertTaskList(taskList);
				}
				TaskListAdapter adapter = new TaskListAdapter(context, taskType);
				adapter.setTaskList(taskList);
				taskListView.setAdapter(adapter);
			} else {
				Toast.makeText(context, R.string.empty_task, Toast.LENGTH_LONG)
						.show();
			}
		} else {
			Toast.makeText(context, R.string.failed_fetch_data,
					Toast.LENGTH_LONG).show();
		}
	}
}
