package com.sdproject.spatialtask.http;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.internal.co;
import com.sdproject.spatialtask.R;
import com.sdproject.spatialtask.adapters.TaskListAdapter;
import com.sdproject.spatialtask.db.TaskDAO;
import com.sdproject.spatialtask.domain.Employee;
import com.sdproject.spatialtask.domain.Task;
import com.sdproject.spatialtask.utilities.AppConstants;
import com.sdproject.spatialtask.utilities.ServiceHandler;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Async task class to get json by making HTTP call
 * */
public class AssignedTask extends AsyncTask<String, String, String> {

	public ProgressDialog pDialog;

	// JSONArray
	JSONArray tasks = null;

	// URL to get contacts JSON
	private static String url = AppConstants.BASE_URL+"Api_employee_task/task/";

	// http://class-note.com/spatial/index.php/Api_employee_task/task?tag=assign_task&emp_id=1&task_id=1

	// JSONArray
	JSONArray contacts = null;

	public static Context context;

	public static final String TAG = "tag";
	public static final String TAG_ID = "id";
	public static final String TAG_DETAILS = "details";
	public static final String TAG_ASSGN_TASK = "assign_task";
	public static final String TAG_EMP_ID = "emp_id";
	public static final String TAG_TASK_ID = "task_id";
	public static final String TAG_SUCCESS = "success";
	int valid;
	List<Task> taskList = new ArrayList<Task>();

	public AssignedTask(Context mContext) {
		context = mContext;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		// Showing progress dialog
		pDialog = new ProgressDialog(context);
		pDialog.setMessage("Please wait...");
		pDialog.setCancelable(false);
		pDialog.show();

	}

	@Override
	protected String doInBackground(String... args) {
		// Creating service handler class instance
		ServiceHandler sh = new ServiceHandler();

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(TAG, TAG_ASSGN_TASK));
		params.add(new BasicNameValuePair(TAG_EMP_ID, args[0]));
		params.add(new BasicNameValuePair(TAG_TASK_ID, args[1]));

		// Making a request to url and getting response
		String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, params);

		if (jsonStr != null) {
			try {
				JSONObject jsonObj = new JSONObject(jsonStr);

				valid = jsonObj.getInt(TAG_SUCCESS);
				if (valid == 1) {
					// TASK IS CREATED
					Log.e("Task is ASSIGNED", "True");
				}
				// Getting JSON Array node

			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			Log.e("ServiceHandler", "Couldn't get any data from the url");
			/* Search Local db if net is not available */
		}

		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		// Dismiss the progress dialog
		if (pDialog.isShowing())
			pDialog.dismiss();

		if (valid == 1) {

			Toast.makeText(context, R.string.created, Toast.LENGTH_LONG)
					.show();

		} else {
			Toast.makeText(context, R.string.failed_fetch_data,
					Toast.LENGTH_LONG).show();
		}
	}
}
