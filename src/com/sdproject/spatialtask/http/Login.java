package com.sdproject.spatialtask.http;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.sdproject.spatialtask.LoginActivity;
import com.sdproject.spatialtask.ManagerActivity;
import com.sdproject.spatialtask.R;
import com.sdproject.spatialtask.domain.Employee;
import com.sdproject.spatialtask.utilities.AppConstants;
import com.sdproject.spatialtask.utilities.ServiceHandler;

/**
 * Async task class to get json by making HTTP call
 * */
public class Login extends AsyncTask<String, String, String> {

	public ProgressDialog pDialog;

	// URL to get contacts JSON
	private static String url = AppConstants.BASE_URL + "Api_login/login/";

	// http://class-note.com/spatial/index.php/Api_login/login?tag=signin&email=faysal@g.com&password=1111

	// JSONArray
	JSONArray contacts = null;

	public static Context context;

	public static final String TAG = "tag";
	public static final String TAG_DETAILS = "details";
	public static final String TAG_SIGNIN = "signin";
	public static final String TAG_EMAIL = "email";
	public static final String TAG_PASSWORD = "password";
	public static final String TAG_EMPLOYEE = "employee";

	public static final String TAG_USERNAME = "username";
	public static final String TAG_ID = "id";
	public static final String TAG_USER_ID = "user_id";
	public static final String TAG_LASTNAME = "lastname";
	public static final String TAG_FIRSTNAME = "firstname";
	public static final String TAG_BIRTH_DATE = "date_of_birth";
	public static final String TAG_DESIGNATION = "designation";
	public static final String TAG_SUCCESS = "success";
	int valid;
	Employee emp;

	public Login(Context mContext) {
		context = mContext;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		// Showing progress dialog
		pDialog = new ProgressDialog(context);
		pDialog.setMessage("Please wait...");
		pDialog.setCancelable(false);
		pDialog.show();

	}

	@Override
	protected String doInBackground(String... args) {
		// Creating service handler class instance
		ServiceHandler sh = new ServiceHandler();

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(TAG, TAG_SIGNIN));
		params.add(new BasicNameValuePair(TAG_PASSWORD, args[0]));
		params.add(new BasicNameValuePair(TAG_EMAIL, args[1]));

		// Making a request to url and getting response
		String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, params);

		if (jsonStr != null) {
			try {
				JSONObject jsonObj = new JSONObject(jsonStr);

				valid = jsonObj.getInt(TAG_SUCCESS);
				if (valid == 1) {
					contacts = jsonObj.getJSONArray(TAG_EMPLOYEE);
					if (contacts.length() > 0) {
						// looping through All Contacts
						for (int i = 0; i < contacts.length(); i++) {
							JSONObject c = contacts.getJSONObject(i);
							emp = new Employee();
							emp.firstname = c.getString(TAG_FIRSTNAME);
							emp.lastname = c.getString(TAG_LASTNAME);
							emp.dateOfBirth = c.getString(TAG_BIRTH_DATE);
							emp.userId = c.getInt(TAG_USER_ID);
							emp.designation = c.getString(TAG_DESIGNATION);
						}
					}
				}
				// Getting JSON Array node
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			Log.e("ServiceHandler", "Couldn't get any data from the url");
			/* Search Local db if net is not available */
		}

		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		// Dismiss the progress dialog
		if (pDialog.isShowing())
			pDialog.dismiss();
		if (valid == 1) {
			
			Intent intent = new Intent(context, LoginActivity.class);
			AppConstants.setBooleanSharedPreference(context,
					AppConstants.SP_LOGGED_IN, true);
			AppConstants.setIntSharedPreference(context,
					AppConstants.SP_USER_ID, emp.userId);
			AppConstants.setStringSharedPreference(context,
					AppConstants.SP_USER_NAME, emp.firstname + " "
							+ emp.lastname);
			AppConstants.setStringSharedPreference(context,
					AppConstants.SP_DESIGNATION, emp.designation);
			context.startActivity(intent);
		} else {
			LoginActivity.mPasswordView.setError(context
					.getString(R.string.error_incorrect_password));
			LoginActivity.mPasswordView.requestFocus();
		}
	}

}
