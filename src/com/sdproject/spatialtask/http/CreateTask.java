package com.sdproject.spatialtask.http;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sdproject.spatialtask.domain.Employee;
import com.sdproject.spatialtask.utilities.AppConstants;
import com.sdproject.spatialtask.utilities.ServiceHandler;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

/**
 * Async task class to get json by making HTTP call
 * */
public class CreateTask extends AsyncTask<String, String, String> {

	public ProgressDialog pDialog;

	// URL to get contacts JSON
	private static String url = AppConstants.BASE_URL+"Api_task/task/";

	// http://class-note.com/spatial/index.php/Api_task/task?tag=create&lat=123.2&lng=444.3&msg=hello&status=pending&deadline=2014-12-02&create_time=2014-11-14&info=hewlsd

	// JSONArray
	JSONArray contacts = null;

	public static Context context;

	public static final String TAG = "tag";
	public static final String TAG_DETAILS = "details";
	public static final String TAG_CREATE = "create";
	public static final String TAG_LAT = "lat";
	public static final String TAG_LNG = "lng";
	public static final String TAG_MSG = "msg";
	public static final String TAG_STATUS = "status";
	public static final String TAG_ID = "id";
	public static final String TAG_DEADLINE = "deadline";
	public static final String TAG_CREATE_TIME = "create_time";
	public static final String TAG_INFO = "info";
	public static final String TAG_SUCCESS = "success";
	int valid;

	public CreateTask(Context mContext) {
		context = mContext;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		// Showing progress dialog
		pDialog = new ProgressDialog(context);
		pDialog.setMessage("Please wait...");
		pDialog.setCancelable(false);
		pDialog.show();

	}

	@Override
	protected String doInBackground(String... args) {
		// Creating service handler class instance
		ServiceHandler sh = new ServiceHandler();

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(TAG, TAG_CREATE));
		params.add(new BasicNameValuePair(TAG_LNG, args[0]));
		params.add(new BasicNameValuePair(TAG_LAT, args[1]));
		params.add(new BasicNameValuePair(TAG_MSG, args[2]));
		params.add(new BasicNameValuePair(TAG_STATUS, args[3]));
		params.add(new BasicNameValuePair(TAG_DEADLINE, args[4]));
		params.add(new BasicNameValuePair(TAG_CREATE_TIME, args[5]));
		params.add(new BasicNameValuePair(TAG_INFO, args[6]));
		

		// Making a request to url and getting response
		String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, params);

		if (jsonStr != null) {
			try {
				JSONObject jsonObj = new JSONObject(jsonStr);

				valid = jsonObj.getInt(TAG_SUCCESS);
				if (valid == 1) {
					//TASK IS CREATED
					Log.e("Task is created","True");
				}
				// Getting JSON Array node

			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			Log.e("ServiceHandler", "Couldn't get any data from the url");
			/* Search Local db if net is not available */
		}

		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		// Dismiss the progress dialog
		if (pDialog.isShowing())
			pDialog.dismiss();
		
		Toast.makeText(context, "Task Created", Toast.LENGTH_SHORT).show();

	}

}
