package com.sdproject.spatialtask.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.sdproject.spatialtask.R;
import com.sdproject.spatialtask.domain.Task;
import com.sdproject.spatialtask.utilities.AppConstants;

public class ManagerDetailsTaskFragment extends SherlockFragment {

	int taskID;
	Task task;
	String taskStatus;
	private Button bAssign,bMap;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.detail_task, container, false);
		Bundle bundle = this.getArguments();
		if (bundle != null) {
			task = new Task();
			task.id = bundle.getInt(AppConstants.IN_TASK_ID, 0);
			task.additionalInfo = bundle.getString(AppConstants.IN_TASK_ADD_INFO);
			task.createTime = bundle.getString(AppConstants.IN_TASK_CREATE_TIME);
			task.deadline = bundle.getString(AppConstants.IN_TASK_DEADLINE);
			task.latitude = bundle.getDouble(AppConstants.IN_TASK_LATTITUDE);
			task.longitude = bundle.getDouble(AppConstants.IN_TASK_LONGITUDE);
			task.message = bundle.getString(AppConstants.IN_TASK_MESSAGE);
			task.status = bundle.getString(AppConstants.IN_TASK_STATUS);
		}
//		TaskDAO taskDao = new TaskDAO(getActivity());
//		task = taskDao.getTask(taskID);

		((TextView) view.findViewById(R.id.tv_message)).setText(getResources()
				.getString(R.string.message) + task.message);
		((TextView) view.findViewById(R.id.tv_status)).setText(getResources()
				.getString(R.string.status) + task.status);
		((TextView) view.findViewById(R.id.tv_deadline)).setText(getResources()
				.getString(R.string.deadline) + task.deadline);
		((TextView) view.findViewById(R.id.tv_create_time)).setText(getResources()
				.getString(R.string.create_time) + task.createTime);
		((TextView) view.findViewById(R.id.tv_additional_info)).setText(getResources()
				.getString(R.string.additional_info) + task.additionalInfo);
		
		bAssign=(Button)view.findViewById(R.id.b_assign);
		bMap=(Button)view.findViewById(R.id.b_map);
		if(task.status.equals(AppConstants.TASK_UNASSIGNED)){
			bAssign.setVisibility(View.VISIBLE);
		}else{
			bAssign.setVisibility(View.GONE);
		}
		bMap.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ShowTaskMapFragment tskMapFrag = new ShowTaskMapFragment();
				Bundle args = new Bundle();
				args.putDouble(AppConstants.IN_TASK_LONGITUDE, task.longitude);
				args.putDouble(AppConstants.IN_TASK_LATTITUDE, task.latitude);
				tskMapFrag.setArguments(args);
				

				FragmentManager fragmentManager = ((SherlockFragmentActivity) getActivity())
						.getSupportFragmentManager();
				fragmentManager.beginTransaction()
						.replace(R.id.fragment_container, tskMapFrag).commit();
			}
		});
		bAssign.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ManagerOtherEmployeeList empFrag = new ManagerOtherEmployeeList();
				Bundle args = new Bundle();
				args.putInt(AppConstants.IN_TASK_ID, task.id);
				empFrag.setArguments(args);
				

				FragmentManager fragmentManager = ((SherlockFragmentActivity) getActivity())
						.getSupportFragmentManager();
				fragmentManager.beginTransaction()
						.replace(R.id.fragment_container, empFrag).commit();
			}
		});

		return view;
	}
	

}
