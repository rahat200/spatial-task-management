package com.sdproject.spatialtask.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sdproject.spatialtask.R;

public class SetMapFragment extends SherlockDialogFragment {

	GoogleMap googleMap;
	MapView mapView;
	Marker oldMarker;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.set_map, container);

		mapView = (MapView) view.findViewById(R.id.mapview);
		mapView.onCreate(savedInstanceState);
		
		setUpMapIfNeeded();
		
		
		
		try {
			MapsInitializer.initialize(this.getActivity());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Updates the location and zoom of the MapView
		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(23.7203, 90.357), 12);
		googleMap.animateCamera(cameraUpdate);

		googleMap.setOnMapClickListener(new OnMapClickListener() {
			
			@Override
			public void onMapClick(LatLng loc) {
				if(oldMarker != null)
					oldMarker.remove();
				
				MarkerOptions marker = new MarkerOptions();
				marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
				marker.position(new LatLng(loc.latitude, loc.longitude));
				oldMarker = googleMap.addMarker(marker);
			}
		});
		
		return view;
		
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		setUpMapIfNeeded();
		super.onActivityCreated(savedInstanceState);
	}

	

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NO_FRAME, 
				android.R.style.Theme_Holo_Light_Dialog);
	}
	
	public GoogleMap getGoogleMap() {

		setUpMapIfNeeded();
		return googleMap;
	}

	private void setUpMapIfNeeded() {
		if (googleMap != null) {
			return;
		}
		 googleMap = mapView.getMap();

	}
	
	
	@Override
	public void onResume() {
		mapView.onResume();
		super.onResume();
	}
 
	@Override
	public void onDestroy() {
		super.onDestroy();
		CreateTaskFragment.latVal = oldMarker.getPosition().latitude;
		CreateTaskFragment.lngVal = oldMarker.getPosition().longitude;
		mapView.onDestroy();
	}
 
	@Override
	public void onLowMemory() {
		super.onLowMemory();
		mapView.onLowMemory();
	}


	

}
