package com.sdproject.spatialtask.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;
import com.sdproject.spatialtask.R;
import com.sdproject.spatialtask.adapters.TaskListAdapter;
import com.sdproject.spatialtask.db.TaskDAO;
import com.sdproject.spatialtask.http.GetTasks;
import com.sdproject.spatialtask.utilities.AppConstants;

public class ManagerUnAssignedTask extends SherlockFragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_tasklist,container,false);
	    
		ListView taskListView = (ListView) view.findViewById(R.id.lv_tasks);
		
		new GetTasks(getActivity(),taskListView).execute(AppConstants.TASK_UNASSIGNED);
//		TaskListAdapter adapter = new TaskListAdapter(getActivity());
//		adapter.setTaskList(new TaskDAO(getActivity()).getTasksList());
//		taskListView.setAdapter(adapter);
		
		return view;
	}
}
