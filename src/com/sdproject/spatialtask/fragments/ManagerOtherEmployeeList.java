package com.sdproject.spatialtask.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.sdproject.spatialtask.R;
import com.sdproject.spatialtask.http.AssignedTask;
import com.sdproject.spatialtask.http.GetEmployees;
import com.sdproject.spatialtask.utilities.AppConstants;

public class ManagerOtherEmployeeList extends SherlockFragment {
	int taskID=0;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_employees, container,
				false);

		ListView empListView = (ListView) view.findViewById(R.id.lv_employees);

		Bundle bundle = this.getArguments();
		if (bundle != null) {
			taskID = bundle.getInt(AppConstants.IN_TASK_ID, 0);
		}
		new GetEmployees(getActivity(), empListView, taskID)
				.execute(AppConstants.EMP_EMPLOYEE);

		// EmployeeListAdapter adapter = new EmployeeListAdapter(getActivity());
		// adapter.setEmployeeList(new
		// EmployeeDAO(getActivity()).getEmployeesList());
		// taskListView.setAdapter(adapter);

		return view;
	}
}
