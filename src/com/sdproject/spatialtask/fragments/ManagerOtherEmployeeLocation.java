package com.sdproject.spatialtask.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sdproject.spatialtask.R;
import com.sdproject.spatialtask.adapters.EmployeeListAdapter;
import com.sdproject.spatialtask.db.EmployeeDAO;


public class ManagerOtherEmployeeLocation extends SherlockFragment{

	GoogleMap googleMap;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_employee_status,container,false);

		setUpMapIfNeeded();
		
		displayEmployeePosition();
		
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		setUpMapIfNeeded();
		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		SupportMapFragment fragment = (SupportMapFragment) (getActivity().getSupportFragmentManager().findFragmentById(R.id.map));
	    //Fragment fragment = (getFragmentManager().findFragmentById(R.id.map));  
	    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
	    ft.remove(fragment);
	    ft.commit();
	    
	}
	
	public GoogleMap getGoogleMap() {
		
		setUpMapIfNeeded();
		return googleMap;
	}
	
	private void setUpMapIfNeeded() {
		if(googleMap != null) {
			return;
		}
		googleMap = ((SupportMapFragment) getActivity().getSupportFragmentManager()
				.findFragmentById(R.id.map)).getMap();
	}
	
	private void displayEmployeePosition() {
		double dummy_lat[] = {23.7256776,23.724828,23.722490,23.717657, 23.7248276};
		double dummy_lng[] = {90.3922161,90.390317,90.393407,90.388643,90.390317};
		
		CameraPosition cameraPosition = new CameraPosition.Builder()
		.target(new LatLng(dummy_lat[0], dummy_lng[0]))
		.zoom(14).build();
		
		googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		
		googleMap.getUiSettings().setAllGesturesEnabled(true);
		
		for(int i = 0; i< 5;i++) {
			MarkerOptions marker = new MarkerOptions().position(
					new LatLng(dummy_lat[i], dummy_lng[i]))
					.title("Mr. mokhles")
					.snippet("near Dhanmondi");
			
			marker.icon(BitmapDescriptorFactory
					.defaultMarker(BitmapDescriptorFactory.HUE_RED));
			
			googleMap.addMarker(marker).showInfoWindow();
		}
	}
}
