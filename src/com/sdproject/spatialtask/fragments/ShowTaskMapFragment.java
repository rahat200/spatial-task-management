package com.sdproject.spatialtask.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sdproject.spatialtask.R;
import com.sdproject.spatialtask.utilities.AppConstants;

public class ShowTaskMapFragment extends SherlockDialogFragment {

	GoogleMap googleMap;
	MapView mapView;
	Marker oldMarker;
	private double lat, lng;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.set_map, container, false);

		Bundle bundle = this.getArguments();
		if (bundle != null) {
			lat = bundle.getDouble(AppConstants.IN_TASK_LATTITUDE);
			lng = bundle.getDouble(AppConstants.IN_TASK_LONGITUDE);
		}

		mapView = (MapView) view.findViewById(R.id.mapview);
		mapView.onCreate(savedInstanceState);

		setUpMapIfNeeded();

		try {
			MapsInitializer.initialize(getActivity());
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		// MapsInitializer.initialize(this.getActivity());

		// Updates the location and zoom of the MapView
		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
				new LatLng(lat, lng), 12);
		googleMap.animateCamera(cameraUpdate);

		if (oldMarker != null)
			oldMarker.remove();

		MarkerOptions marker = new MarkerOptions();
		marker.icon(BitmapDescriptorFactory
				.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
		marker.position(new LatLng(lat, lng));
		oldMarker = googleMap.addMarker(marker);

		return view;

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		setUpMapIfNeeded();
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NO_FRAME,
				android.R.style.Theme_Holo_Light_Dialog);
	}

	public GoogleMap getGoogleMap() {

		setUpMapIfNeeded();
		return googleMap;
	}

	private void setUpMapIfNeeded() {
		if (googleMap != null) {
			return;
		}
		googleMap = mapView.getMap();

	}

	@Override
	public void onResume() {
		mapView.onResume();
		super.onResume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mapView.onDestroy();
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		mapView.onLowMemory();
	}

}
