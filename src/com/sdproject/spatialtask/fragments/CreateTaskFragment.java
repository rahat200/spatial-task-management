package com.sdproject.spatialtask.fragments;

import java.util.Calendar;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.actionbarsherlock.app.SherlockFragment;
import com.sdproject.spatialtask.R;
import com.sdproject.spatialtask.http.CreateTask;
import com.sdproject.spatialtask.utilities.AppConstants;

public class CreateTaskFragment extends SherlockFragment {

	private EditText etMessage, etInfo;
	private int year, month, day,dDay,dMonth,dYear;

	private DatePicker dateDeadline;
	private OnDateChangedListener dateSetListener;
	private Button bCreate;
	
	public static double latVal = 0;
	public static double lngVal = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.create_task,container,false);
		etMessage = (EditText) view.findViewById(R.id.et_message);
		etInfo = (EditText) view.findViewById(R.id.et_info);

		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);

		// set current date into datepicker
		dateDeadline = (DatePicker) view.findViewById(R.id.date_deadline);
		DatePicker.OnDateChangedListener dateSetListener = new DatePicker.OnDateChangedListener() {

		    public void onDateChanged(DatePicker view, int year, int monthOfYear,
		            int dayOfMonth) {
		    	dDay =dayOfMonth;
		    	dMonth =monthOfYear;
		    	dYear = year;

		    }
		};
//		dateDeadline.init(year, month, day, dateSetListener);
		Button btn_setmap = (Button) view.findViewById(R.id.btn_map);
		Button btn_create = (Button) view.findViewById(R.id.b_create);
		
		btn_setmap.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				SetMapFragment frag = new SetMapFragment();
				frag.show(getActivity().getSupportFragmentManager(), "SetMap");
				
			}
		});
		
		btn_create.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				dDay = 11;
				dMonth = 10;
				dYear = 2014;
				
				String msg = etMessage.getText().toString();
				String info = etInfo.getText().toString();
				String deadline = dDay + "-" + dMonth + "-" + dYear;
				String createTime = dDay+2 + "-" + dMonth + "-" + dYear;
				
				new CreateTask(getActivity()).execute(lngVal + "", latVal + "", msg,
						AppConstants.TASK_PENDING, deadline, createTime, info);

				
			}
		});
	
		

		return view;
	}



	public void onCreateTaskClicked(View view) {
		String msg = etMessage.getText().toString();
		String info = etInfo.getText().toString();
		double lng = 18.22;
		double lat = 22.22;
		
		day =11;
		month=11;
		year=2014;

		dDay =11;
		dMonth=11;
		dYear=2014;
		
		String createTime = day + "-" + month + "-" + year;
//		int dDay = dateDeadline.getDayOfMonth();
//		int dMonth = dateDeadline.getMonth();
//		int dYear = dateDeadline.getYear();
		String deadline = dDay + "-" + dMonth + "-" + dYear;

		new CreateTask(getActivity()).execute(lng + "", lat + "", msg,
				AppConstants.TASK_PENDING, deadline, createTime, info);

	}
}
